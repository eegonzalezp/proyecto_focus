<?php
$correo = $_POST["correo"];
$clave = $_POST["clave"];
$administrador = new Administrador("", "", "", $correo, $clave);
$artista = new Artista("", "", "", $correo, $clave);
$critico = new Critico("", "", "", $correo, $clave);
$analista = new Analista("", "", "", $correo, $clave);

if ($administrador->autenticar()) {
    $_SESSION["id"] = $administrador->getIdAdministrador();
    $_SESSION["rol"] = "Administrador";
    $_SESSION["correo"] = $correo;

    $administrador = new Administrador($_SESSION["id"]);
    $administrador->consultar();
    $log = new Log("", "Inicio sesion","Inició sesión" . "-" . $administrador->getNombre() . " " . $administrador->getApellido() . "-" . "ADMINISTRADOR" . "-" . $correo, "NOW()", "NOW()", $correo);
    $log->insertar();

    header("Location: index.php?pid=" . base64_encode("Presentacion/administrador/sesionAdministrador.php"));
} else if ($artista->autenticar()) {
    if ($artista->getEstado() == -1) {
        header("Location: index.php?error=2");
    } else if ($artista->getEstado() == 0) {
        header("Location: index.php?error=3");
    } else {
        $_SESSION["id"] = $artista->getIdArtista();
        $_SESSION["rol"] = "Artista";
        $_SESSION["correo"] = $correo;

        $artista = new Artista($_SESSION["id"]);
        $artista->consultar();
        $log = new Log("", "Inicio sesion","Inició sesión" . "-" . $artista->getNombre() . " " . $artista->getApellido() . "-" . "ARTISTA" . "-" . $correo, "NOW()", "NOW()", $correo);
        $log->insertar();

        header("Location: index.php?pid=" . base64_encode("Presentacion/artista/sesionArtista.php"));
    }
} else if ($critico->autenticar()) {

    $_SESSION["id"] = $critico->getIdCritico();
    $_SESSION["rol"] = "Critico";
    $_SESSION["correo"] = $correo;

    $critico = new Critico($_SESSION["id"]);
    $critico->consultar();
    $log = new Log("", "Inicio sesion","Inició sesión" . "-" . $critico->getNombre() . " " . $critico->getApellido() . "-" . "CRITICO" . "-" . $correo, "NOW()", "NOW()", $correo);
    $log->insertar();

    header("Location: index.php?pid=" . base64_encode("Presentacion/critico/sesionCritico.php"));
} else if ($analista->autenticar()) {

    $_SESSION["id"] = $analista->getIdAnalista();
    $_SESSION["rol"] = "Analista";
    $_SESSION["correo"] = $correo;

    $analista = new Analista($_SESSION["id"]);
    $analista->consultar();
    $log = new Log("", "Inicio sesion","Inició sesión" . "-" . $analista->getNombre() . " " . $analista->getApellido() . "-" . "ANALISTA" . "-" . $correo, "NOW()", "NOW()", $correo);
    $log->insertar();

    header("Location: index.php?pid=" . base64_encode("Presentacion/analista/sesionAnalista.php"));
} else {
    header("Location: index.php?error=1");
}
